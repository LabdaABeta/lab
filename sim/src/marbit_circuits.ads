package Marbit_Circuits is
    pragma Pure;

    type Marbit_Circuit (<>) is private;

    function Empty_Circuit return Marbit_Circuit;

    -- Vertical paste
    function "+" (To : Marbit_Circuit; Line : String) return Marbit_Circuit;
    function "+" (Line : String; To : Marbit_Circuit) return Marbit_Circuit;

    -- Horizontal paste - must have same height
    function "&" (To : Marbit_Circuit; Column : String) return Marbit_Circuit;

    function Minimize (From : Marbit_Circuit) return Marbit_Circuit;

    function Input_Count (From : Marbit_Circuit) return Natural;
    function Line_Count (From : Marbit_Circuit) return Natural;
    function Size (From : Marbit_Circuit) return Natural;

    -- Recommended to minimize the circuit first!
    type Bit_Vector is array (Positive range <>) of Boolean;
    procedure Apply (This : Marbit_Circuit; To : in out Bit_Vector);
    procedure Apply_Full (This : Marbit_Circuit; To : in out Bit_Vector);

    -- This expands !file references, may raise constraint error
    function Expand (
        From : Marbit_Circuit;
        Using : access function (Name : String) return Marbit_Circuit)
        return Marbit_Circuit;

    procedure Show (
        What : Marbit_Circuit;
        Using : access procedure (Line : String));
private
    type Line_Kind is (INPUT_LINE, GARBAGE_LINE, NO_LINE);
    type Line_Kind_List is array (Positive range <>) of Line_Kind;

    type Step_Kind is (NORMAL_STEP, CONTROL_STEP, ACT_STEP, INCL_STEP);
    type Step_Array is array (Positive range <>) of Step_Kind;
    type Step_List is array (Positive range <>, Positive range <>) of Step_Kind;

    type Character_Matrix is array (Positive range <>, Positive range <>) of
        Character;

    type Marbit_Circuit (Height, Width : Natural) is
        record
            Lines : Line_Kind_List (1 .. Height);
            Steps : Step_List (1 .. Width, 1 .. Height);
            Raw : Character_Matrix (1 .. Height, 1 .. Width);
        end record;
end Marbit_Circuits;
