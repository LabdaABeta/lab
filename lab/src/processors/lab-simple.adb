package body LAB.Simple is

    procedure Step (State : in out Simple_Processor; Bus : in out CPU_Bus) is
    begin
        -- Clear any operations on interrupt
        if Bus.Status.Ackn then
            State.State := Postfetch;
            Bus.Status := (Code => True, Ackn => True, others => False);
            Bus.Fetch := Bus.Handler;
            return;
        end if;

        case State is
            when Prefetch =>
                State.State := Execute;
                Bus.Status := (Code => True, others => False);
                Bus.Fetch := State.Registers (P);

            when Execute =>
                if not Bus.Status.Code then
                    Bus.Status := (others => False);
                    return;
                end if;

                if not Bus.Valid then
                    State.State := Prefetch;
                    return;
                end if;

                declare
                    Op : Operation := Parse (Bus.Instruction);
                    Inc : PC_Increment := Bus.PC_Increment;
                    That : Word := Apply_Shift (
                        To => State.Registers (Op.Second),
                        Plus => Op.Immediate,
                        By => Op.Shift);
                    This : Word := State.Registers (Op.First);
                begin
                    State.Target := Op.First;
                    State.State := Writeback;
                    Bus.Status := (others => False);
                    State.Result := (case Op.Operation is
                        when OP_ADD => This + That,
                        when OP_MUL => This * That,
                        when OP_AND => This and That,
                        when OP_ORR => This or That,
                        when OP_XOR => This xor That,
                        when OP_SUB => This - That,
                        when others => This);
                    State.Branch := (case Op.Operation is
                        when OP_ADD .. OP_SUB => False,
                        when OP_BLT => This < 0,
                        when OP_BEQ => This = 0,
                        when OP_BLE => This <= 0,
                        when OP_BGT => This > 0,
                        when OP_BNE => This /= 0,
                        when OP_BGE => This >= 0,
                        when OP_JAL => True);

                    if Op.Operation = OP_STR then
                        Bus.Status := (Data => True, others => False);
                        Bus.Address := That;
                        Bus.Value := This;
                        State.Data := True;
                    end if;

                    if Op.Operation = OP_LDA then
                        Bus.Status := (
                            Data => True, Read => True, others => False);
                        Bus.Address := That;
                        State.Data := True;
                    end if;
                end;

            when Writeback =>
                if State.Data then
                    if not Bus.Data then
                        Bus.Status := (others => False);
                        return;
                    end if;

                    State.Result := Bus.Datum;
                end if;

                if State.Target /= Zero and State.Target /= P then
                    State.Registers (State.Target) := State.Result;
                end if;

                if State.Branch then
                    State.Registers (P) := State.Result;
                end if;

                State.State := Execute;
                Bus.Status := (Code => True, others => False);
                Bus.Fetch := State.Registers (P);
        end case;
    end Step;

end LAB.Simple;
