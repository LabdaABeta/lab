with Buffers;
with Source; use Source;

package body Lining is
    task body Liner is
        Last : Character;
        Follow : Character;
    begin
        loop
            select
                accept Line;
            or
                terminate;
            end select;
            loop
                Input.Remove (Last);

                if Last = '\' then
                    Input.Remove (Follow);

                    if Follow /= ASCII.LF then
                        Output.Insert (Last);
                        Output.Insert (Follow);
                    end if;
                else
                    Output.Insert (Last);
                end if;

                exit when Last = Character'First;
            end loop;
        end loop;
    end Liner;
end Lining;
