with Marbit_Circuits; use Marbit_Circuits;
with Ada.Text_IO; use Ada.Text_IO;

procedure MMinimize is
    function Read_Circuit return Marbit_Circuit is
    begin
        if End_Of_File then
            return Empty_Circuit;
        else
            declare
                Line : String := Get_Line;
            begin
                return Line + Read_Circuit;
            end;
        end if;
    end Read_Circuit;

    Circuit : Marbit_Circuit := Read_Circuit;
    Minimized : Marbit_Circuit := Minimize (Circuit);
begin
    Show (Minimized, Put_Line'Access);
end MMinimize;
