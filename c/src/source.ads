with Ada.Strings.Unbounded;

package Source is
    -- Most processing is done in terms of source characters.
    -- These encode value and location.
    type Character is
        record
            File_Name : Ada.Strings.Unbounded_String;
            Row       : Positive;
            Column    : Positive;
            Value     : Standard.Character;
        end record;

    function "=" (Left, Right : Character) return Boolean;
    function "=" (Left : Character; Right : Standard.Character) return Boolean;
    function "=" (Left : Standard.Character; Right : Character) return Boolean;
    function Image (C : Character) return Standard.String;
end Source;

