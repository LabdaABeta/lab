with Marbit_Circuits; use Marbit_Circuits;
with Ada.Command_Line; use Ada.Command_Line;
with Ada.Text_IO; use Ada.Text_IO;
with Ada.Containers.Doubly_Linked_Lists;
with Ada.Containers.Indefinite_Doubly_Linked_Lists;

with Ada.Exceptions;

-- Each argument is input->output
-- EG: 110XX-001X0
-- All must have same height - the tested circuit height
-- Will try to find the smallest width circuit that satisfies via brute force
-- Will print said circuit
procedure Bruteforcer is
    Clear_Screen : constant String :=
        Character'Val (27) & "[2J" &
        Character'Val (27) & "[H";

    Height : Positive := (Argument (1)'Length - 1) / 2;

    type Result_Type is (RESULT_ONE, RESULT_ZERO, RESULT_DONT_CARE);

    subtype Test_Input_Vector is Bit_Vector (1 .. Height);
    type Test_Output_Vector is array (1 .. Height) of Result_Type;

    type Test_Vector is
        record
            Input : Test_Input_Vector;
            Output : Test_Output_Vector;
        end record;

    package Test_Vector_Lists is new
        Ada.Containers.Doubly_Linked_Lists (Test_Vector);
    use Test_Vector_Lists;

    function Read_Vector (From : String) return Test_Vector is
        Result : Test_Vector;

        Index : Positive := From'First;
    begin
        for X of Result.Input loop
            X := From (Index) = '1';
            Index := Index + 1;
        end loop;

        Index := Index + 1;

        for X of Result.Output loop
            if From (Index) = '1' then
                X := RESULT_ONE;
            elsif From (Index) = '0' then
                X := RESULT_ZERO;
            else
                X := RESULT_DONT_CARE;
            end if;

            Index := Index + 1;
        end loop;

        return Result;
    end Read_Vector;

    subtype Column_Representation is String (1 .. Height);

    package Column_Lists is new
        Ada.Containers.Doubly_Linked_Lists (Column_Representation);

    function Get_Row (From : Column_Lists.List; Row : Positive) return String is
        Result : String (1 .. Integer (From.Length));
        Cursor : Column_Lists.Cursor := From.First;
    begin
        for C of Result loop
            C := Column_Lists.Element (Cursor)(Row);
            Column_Lists.Next (Cursor);
        end loop;

        return ">" & Result;
    end Get_Row;

    function Make_Circuit (From : Column_Lists.List) return Marbit_Circuit is
        function Rec_Make_Circuit (Row : Positive) return Marbit_Circuit is
        begin
            if Row > Height then
                return Empty_Circuit;
            else
                return Get_Row (From, Row) + Rec_Make_Circuit (Row + 1);
            end if;
        end Rec_Make_Circuit;
    begin
        return Rec_Make_Circuit (1);
    end Make_Circuit;

    function Check_Circuit (
        Which : Marbit_Circuit;
        Tests : Test_Vector_Lists.List)
        return Boolean is
    begin
        for Test of Tests loop
            declare
                Result : Test_Input_Vector := Test.Input;
            begin
                Apply (Which, Result);

                for I in Test.Output'Range loop
                    case Test.Output (I) is
                        when RESULT_ONE =>
                            if not Result (I) then
                                return False;
                            end if;
                        when RESULT_ZERO =>
                            if Result (I) then
                                return False;
                            end if;
                        when RESULT_DONT_CARE =>
                            null;
                    end case;
                end loop;
            end;
        end loop;

        return True;
    end Check_Circuit;

    function Generate_Column_Set return Column_Lists.List is
        Result : Column_Lists.List := Column_Lists.Empty_List;
        Seed : Column_Representation := (others => '?');

        -- Loads each combination of controls in ? spots
        procedure Load_Column (From : Column_Representation) is
            Add_Item : Column_Representation := From;
            Found_Space : Boolean := False;
        begin
            for I in Add_Item'Range loop
                if Add_Item (I) = '?' then
                    Add_Item (I) := '*';
                    Load_Column (Add_Item);
                    Add_Item (I) := ' ';
                    Load_Column (Add_Item);
                    Add_Item (I) := '?';

                    Found_Space := True;
                end if;
            end loop;

            if not Found_Space then
                Result.Append (From);
            end if;
        end Load_Column;
    begin
        -- Add each C-NOT
        for I in Seed'Range loop
            Seed (I) := '@';

            -- Add each C-SWAP
            for J in Positive range I + 1 .. Seed'Last loop
                Seed (J) := '@';
                Load_Column (Seed);
                Seed (J) := '?';
            end loop;

            Load_Column (Seed);
            Seed (I) := '?';
        end loop;

        return Result;
    end Generate_Column_Set;

    function Root_Circuit (
        Root : Marbit_Circuit := Empty_Circuit;
        Remaining : Natural := Height)
        return Marbit_Circuit is
    begin
        if Remaining > 0 then
            return Root_Circuit (Root + ">", Remaining - 1);
        else
            return Root;
        end if;
    end Root_Circuit;

    Column_Options : Column_Lists.List := Generate_Column_Set;
    Iteration : Positive := 1;

    Tests : Test_Vector_Lists.List := Test_Vector_Lists.Empty_List;
    Columns : Column_Lists.List := Column_Lists.Empty_List;

    -- Searches up to the given depth, returns a non-empty circuit if found
    function Depth_First_Search (
        Root : Marbit_Circuit;
        Depth : Natural)
        return Marbit_Circuit is

    begin
        if Depth = 0 then
            Put_Line (Clear_Screen);
            Put_Line ("Trying iteration" & Iteration'Img &
                " (branching factor" & Column_Options.Length'Img & ")");

            Iteration := Iteration + 1;

            Show (Root, Put_Line'Access);

            if Check_Circuit (Root, Tests) then
                return Root;
            else
                return Empty_Circuit;
            end if;
        end if;

        for Additional of Column_Options loop
            declare
                Candidate : Marbit_Circuit := Depth_First_Search (
                    Root & Additional,
                    Depth - 1);
            begin
                if Size (Candidate) > 0 then
                    return Candidate;
                end if;
            end;
        end loop;

        return Empty_Circuit;
    end Depth_First_Search;

    Depth : Natural := 0;
begin
    for I in Positive range 1 .. Argument_Count loop
        Tests.Append (Read_Vector (Argument (I)));
    end loop;

    loop
        declare
            Candidate : Marbit_Circuit :=
                Depth_First_Search (Root_Circuit, Depth);
        begin
            if Size (Candidate) > 0 then
                Put_Line ("");
                Put_Line ("This looks like it satisfies it!");

                return;
            end if;
        end;

        Depth := Depth + 1;
    end loop;
end Bruteforcer;
