with Interfaces;
with Ada.Unchecked_Conversion;

package body LAB is
    subtype I64 is Interfaces.Integer_64;
    subtype U64 is Interfaces.Unsigned_64;

    function I2U is new Ada.Unchecked_Conversion (I64, U64);
    function U2I is new Ada.Unchecked_Conversion (U64, I64);

    function "and" (Left, Right : Word) return Word is (
        Word (U2I (I2U (I64 (Left)) and I2U (I64 (Right)))));

    function "or" (Left, Right : Word) return Word is (
        Word (U2I (I2U (I64 (Left)) or I2U (I64 (Right)))));

    function "xor" (Left, Right : Word) return Word is (
        Word (U2I (I2U (I64 (Left)) xor I2U (I64 (Right)))));

    S2B : constant array (Bit .. Dinner) of Integer := (Bit => 1,
        Crumb => 2, Nibble => 4, Byte => 8, Plate => 16, Dinner => 32);

    function Do_Shift (To : U64; By : Shift_Value) return U64 is (
        if By.Direction = Right then (
            case By.Amount is
                when Special => Shift_Right (To, 1),
                when Circular_Bit => Rotate_Right (To, 1),
                when others => Shift_Right_Arithmetic (To, S2B (By.Amount)))
        else (
            case By.Amount is
                when Special => To,
                when Circular_Bit => Rotate_Left (To, 1),
                when others => Shift_Left (To, S2B (By.Amount))))

    function Apply_Shift (To : Word; Plus : Immediate_Value; By : Shift_Kind)
        return Word is (
            Word (U2I (Do_Shift (I2U (I64 (To + Word (Plus))), By))));

    function Parse (Instruction : Operation_Word) return Operation is (
        First => Register_Index'Val (
            (Instruction / (2 ** 20)) mod (2 ** 4)),
        Second => Register_Index'Val (
            (Instruction / (2 ** 16)) mod (2 ** 4)),
        Immediate => (
            if Instruction mod (2 ** 16) > 2 ** 15 then
                (Instruction mod (2 ** 16)) - 2 ** 17
            else
                Instruction mod (2 ** 16)),
        Shift => (
            Direction => Shift_Direction'Val (
                (Instruction / (2 ** 27)) mod 2),
            Amount => Shift_Amount'Val (
                (Instruction / (2 ** 24)) mod (2 ** 3))),
        Operation => Operation_Kind'Val (
            Instruction / (2 ** 28)));
end LAB;
