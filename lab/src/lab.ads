package LAB is
    ----------------------------------------------------------------------------
    -- Interface Specification
    ----------------------------------------------------------------------------

    type Word is range -(2 ** 63) .. +(2 ** 63 - 1);
    type Operation_Word is mod 2 ** 32;

    type Memory_Increment is range 1 .. 32;

    -- State is input after a step, output before a step
    type Bus_State is (Input, Output);

    type Bus_Status is
        record
            Code : Boolean; -- Whether code bus has information
            Data : Boolean; -- Whether data bus has information
            Read : Boolean; -- Whether data information is read/write
            Ackn : Boolean; -- Whether interrupt bus has information
        end record;

    -- Ensure that you respond to write commands with the value written in Datum
    type CPU_Bus (State : Bus_State := Input) is
        record
            Status : Bus_Status;

            case State is
                when Input =>
                    Instruction : Operation_Word; ----\
                    PC_Increment : Memory_Increment; --- Code Bus
                    Valid : Boolean; -----------------/
                    Datum : Word; ---------------------- Data Bus
                    Handler : Word; -------------------- Interrupt Bus

                when Output =>
                    Fetch : Word; ---------------------- Code Bus
                    Address : Word; -------------------- Data Bus
                    Value : Word; ---------------------- Data Bus
            end case;
        end record;

    generic
        type Processor_Internals (<>) is limited private;
        with procedure Step (
            State : in out Processor_Internals;
            Bus : in out CPU_Bus);
    package CPU_Implementation;

    ----------------------------------------------------------------------------
    -- Interface Helper Utilities
    ----------------------------------------------------------------------------

    type Register_Index is (Zero, A, B, C, I, J, K, M, N, X, Y, Z, F, S, L, P);
    type Register_Page is array (Register_Index) of Word;

    type Immediate_Value is range -(2 ** 15) .. +(2 ** 15 - 1);

    type Shift_Direction is (Right, Left);
    type Shift_Amount is ( -- Special = 0 for Left, Logical Bit for Right
        Special, Bit, Crumb, Nibble, Byte, Plate, Dinner, Circular_Bit);
    type Shift_Value is
        record
            Direction : Shift_Direction;
            Amount    : Shift_Amount;
        end record;

    type Operation_Kind is (OP_ADD, OP_MUL, OP_AND, OP_ORR, OP_XOR, OP_MOV,
        OP_STR, OP_LDA, OP_SUB, OP_BLT, OP_BEQ, OP_BLE, OP_BGT, OP_BNE, OP_BGE,
        OP_JAL);

    type Operation is
        record
            First : Register_Index;
            Second : Register_Index;
            Immediate : Immediate_Value;
            Shift : Shift_Value;
            Operation : Operation_Kind;
        end record;

    function "and" (Left, Right : Word) return Word;
    function "or" (Left, Right : Word) return Word;
    function "xor" (Left, Right : Word) return Word;
    function Apply_Shift (To : Word; Plus : Immediate_Value; By : Shift_Value)
        return Word;
    -- TODO: Add partial multiplication operations

    function Parse (Instruction : Operation_Word) return Operation;

    ----------------------------------------------------------------------------
    -- Representation clauses
    ----------------------------------------------------------------------------

    for Bus_Status use record
        Code at 0 range 0 .. 0;
        Data at 0 range 1 .. 1;
        Read at 0 range 2 .. 2;
        Ackn at 0 range 3 .. 3;
    end record;
    for Bus_Status'Size use 4;

    pragma Unchecked_Union (CPU_Bus);

    for CPU_Bus use record
        Status at 0 range 192 .. 195;
        Instruction at 0 range 0 .. 31;
        PC_Increment at 0 range 32 .. 37;
        Valid at 0 range 63 .. 63;
        Datum at 0 range 64 .. 127;
        Handler at 0 range 128 .. 191;
        Fetch at 0 range 0 .. 63;
        Address at 0 range 64 .. 127;
        Value at 0 range 128 .. 191;
    end record;
    for CPU_Bus'Size use 196;

    for Shift_Value use record
        Direction at 0 range 3 .. 3;
        Amount at 0 range 0 .. 2;
    end record;
    for Shift_Value'Size use 4;
end LAB;
