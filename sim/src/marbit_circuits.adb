package body Marbit_Circuits is
    From_Line : constant array (Line_Kind) of Character := (
        INPUT_LINE => '>',
        GARBAGE_LINE => '%',
        NO_LINE => ' ');

    function To_Step (From : Character) return Step_Kind is begin
        case From is
            when '*' => return CONTROL_STEP;
            when '@' => return ACT_STEP;
            when others => return NORMAL_STEP;
        end case;
    end To_Step;

    From_Step : constant array (Step_Kind) of Character := (
        NORMAL_STEP => ' ',
        CONTROL_STEP => '*',
        ACT_STEP => '@',
        INCL_STEP => '@');

    function Empty_Circuit return Marbit_Circuit is
        Result : Marbit_Circuit := (
            Height => 0,
            Width => 0,
            Lines => (others => NO_LINE),
            Steps => (others => (others => NORMAL_STEP)),
            Raw => (others => (others => ' ')));
    begin
        return Result;
    end Empty_Circuit;

    function Get_Line_Type (Line : String) return Line_Kind is begin
        for I in Line'Range loop
            case Line (I) is
                when '>' => return INPUT_LINE;
                when '%' => return GARBAGE_LINE;
                when others => null;
            end case;
        end loop;

        return NO_LINE;
    end Get_Line_Type;

    function Get_Line_Steps (
        Line : String;
        Check_Line : Boolean := True)
        return Step_Array
    is
        Result : Step_Array (Line'Range) := (others => NORMAL_STEP);

        Is_Include : Boolean := False;
    begin
        if Check_Line and Get_Line_Type (Line) = NO_LINE then
            return Result;
        end if;

        for I in Line'Range loop
            if Is_Include then
                Result (I) := INCL_STEP;
                Is_Include := Line (I) /= '#';
            else
                if Line (I) = '#' then
                    Is_Include := True;
                    Result (I) := INCL_STEP;
                else
                    Result (I) := To_Step (Line (I));
                end if;
            end if;
        end loop;

        return Result;
    end Get_Line_Steps;

    function "+" (To : Marbit_Circuit; Line : String) return Marbit_Circuit is
        Result : Marbit_Circuit := (
            Height => To.Height + 1,
            Width => Natural'Max (To.Width, Line'Length),
            Lines => (others => NO_LINE),
            Steps => (others => (others => NORMAL_STEP)),
            Raw => (others => (others => ' ')));

        Steps : Step_Array := Get_Line_Steps (Line);
    begin
        Result.Lines (1 .. To.Height) := To.Lines;
        Result.Lines (To.Height + 1) := Get_Line_Type (Line);

        for X in Positive range 1 .. Result.Width loop
            for Y in Positive range 1 .. To.Height loop
                if X <= To.Width then
                    Result.Steps (X, Y) := To.Steps (X, Y);
                    Result.Raw (Y, X) := To.Raw (Y, X);
                else
                    Result.Steps (X, Y) := NORMAL_STEP;
                    Result.Raw (Y, X) := ' ';
                end if;
            end loop;

            if X <= Line'Length then
                Result.Steps (X, To.Height + 1) :=
                    Steps (Steps'First + X - 1);
                Result.Raw (To.Height + 1, X) := Line (Line'First + X - 1);
            else
                Result.Steps (X, To.Height + 1) := NORMAL_STEP;
                Result.Raw (To.Height + 1, X) := ' ';
            end if;
        end loop;

        return Result;
    end "+";

    function "+" (Line : String; To : Marbit_Circuit) return Marbit_Circuit is
        Result : Marbit_Circuit := (
            Height => To.Height + 1,
            Width => Natural'Max (To.Width, Line'Length),
            Lines => (others => NO_LINE),
            Steps => (others => (others => NORMAL_STEP)),
            Raw => (others => (others => ' ')));

        Steps : Step_Array := Get_Line_Steps (Line);
    begin
        Result.Lines (2 .. Result.Height) := To.Lines;
        Result.Lines (1) := Get_Line_Type (Line);

        for X in Positive range 1 .. Result.Width loop
            for Y in Positive range 1 .. To.Height loop
                if X <= To.Width then
                    Result.Steps (X, Y + 1) := To.Steps (X, Y);
                    Result.Raw (Y + 1, X) := To.Raw (Y, X);
                else
                    Result.Steps (X, Y + 1) := NORMAL_STEP;
                    Result.Raw (Y + 1, X) := ' ';
                end if;
            end loop;

            if X <= Line'Length then
                Result.Steps (X, 1) := Steps (Steps'First + X - 1);
                Result.Raw (1, X) := Line (Line'First + X - 1);
            else
                Result.Steps (X, 1) := NORMAL_STEP;
                Result.Raw (1, X) := ' ';
            end if;
        end loop;

        return Result;
    end "+";

    function "&" (To : Marbit_Circuit; Column : String) return Marbit_Circuit is
        Result : Marbit_Circuit := (
            Height => To.Height,
            Width => To.Width + 1,
            Lines => To.Lines,
            Steps => (others => (others => NORMAL_STEP)),
            Raw => (others => (others => ' ')));

        Steps : Step_Array := Get_Line_Steps (Column, False);
    begin
        if Column'Length /= To.Height then
            return To;
        end if;

        for X in Positive range 1 .. To.Width loop
            for Y in Positive range 1 .. To.Height loop
                Result.Steps (X, Y) := To.Steps (X, Y);
                Result.Raw (Y, X) := To.Raw (Y, X);
            end loop;
        end loop;

        for Y in Positive range 1 .. To.Height loop
            Result.Steps (To.Width + 1, Y) := Steps (Y);
            Result.Raw (Y, To.Width + 1) := Column (Column'First + Y - 1);
        end loop;

        return Result;
    end "&";

    function Minimize (From : Marbit_Circuit) return Marbit_Circuit is
        Dead_Rows : array (From.Lines'Range) of Boolean := (others => False);
        Dead_Columns : array (From.Steps'Range (1)) of Boolean := (
            others => False);

        Row_Casualties : Natural := 0;
        Column_Casualties : Natural := 0;
    begin
        -- Mark unused rows
        for Y in From.Lines'Range loop
            if From.Lines (Y) = NO_LINE then
                Dead_Rows (Y) := True;
                Row_Casualties := Row_Casualties + 1;
            end if;
        end loop;

        -- Mark unused columns
        for X in From.Steps'Range (1) loop
            if (
                for all Y in From.Steps'Range (2) =>
                    From.Steps (X, Y) = NORMAL_STEP)
            then
                Dead_Columns (X) := True;
                Column_Casualties := Column_Casualties + 1;
            end if;
        end loop;

        declare
            Result : Marbit_Circuit := (
                Height => From.Height - Row_Casualties,
                Width => From.Width - Column_Casualties + 1,
                Lines => (others => NO_LINE),
                Steps => (others => (others => NORMAL_STEP)),
                Raw => (others => (others => ' ')));
            Source_Row : Positive := 1;
            Source_Column : Positive := 1;
        begin
            -- Populate I/O
            for Y in Positive range 1 .. Result.Height loop
                while Dead_Rows (Source_Row) loop
                    Source_Row := Source_Row + 1;
                end loop;

                Result.Lines (Y) := From.Lines (Source_Row);

                Result.Steps (1, Y) := NORMAL_STEP;
                Result.Raw (Y, 1) := From_Line (Result.Lines (Y));

                Source_Row := Source_Row + 1;
            end loop;

            for X in Positive range 2 .. Result.Width loop
                while Dead_Columns (Source_Column) loop
                    Source_Column := Source_Column + 1;
                end loop;

                Source_Row := 1;

                for Y in Positive range 1 .. Result.Height loop
                    while Dead_Rows (Source_Row) loop
                        Source_Row := Source_Row + 1;
                    end loop;

                    Result.Steps (X, Y) := From.Steps (
                        Source_Column, Source_Row);

                    if Result.Steps (X, Y) = NORMAL_STEP then
                        Result.Raw (Y, X) := ' ';
                    else
                        Result.Raw (Y, X) :=
                            From.Raw (Source_Row, Source_Column);
                    end if;

                    Source_Row := Source_Row + 1;
                end loop;

                Source_Column := Source_Column + 1;
            end loop;

            return Result;
        end;
    end Minimize;

    function Input_Count (From : Marbit_Circuit) return Natural is
        Result : Natural := 0;
    begin
        for Line of From.Lines loop
            if Line = INPUT_LINE then
                Result := Result + 1;
            end if;
        end loop;

        return Result;
    end Input_Count;

    function Line_Count (From : Marbit_Circuit) return Natural is
        Result : Natural := 0;
    begin
        for Line of From.Lines loop
            if Line /= NO_LINE then
                Result := Result + 1;
            end if;
        end loop;

        return Result;
    end Line_Count;

    function Size (From : Marbit_Circuit) return Natural is
    begin
        return From.Width * From.Height;
    end Size;

    procedure Do_Apply (This : Marbit_Circuit; State : in out Bit_Vector) is
        Controlled : Boolean;
        First, Second : Natural;
        Temp : Boolean;
    begin
        -- Mutate the state
        for X in Positive range 1 .. This.Width loop
            Controlled := True;
            First := 0;
            Second := 0;

            for Y in Positive range 1 .. This.Height loop
                case This.Steps (X, Y) is
                    when CONTROL_STEP =>
                        if not State (Y) then
                            Controlled := False;
                        end if;
                    when ACT_STEP =>
                        if First = 0 then
                            First := Y;
                        elsif Second = 0 then
                            Second := Y;
                        else
                            raise Constraint_Error;
                        end if;
                    when others =>
                        null;
                end case;
            end loop;

            if Controlled then
                -- Update the state
                if Second /= 0 then
                    Temp := State (Second);
                    State (Second) := State (First);
                    State (First) := Temp;
                elsif First /= 0 then
                    State (First) := not State (First);
                end if;
            end if;
        end loop;
    end Do_Apply;

    procedure Apply (This : Marbit_Circuit; To : in out Bit_Vector) is
        State : Bit_Vector (1 .. This.Height) := (others => False);

        Current_Datum : Positive := To'First;
    begin
        -- Initialize the state
        for Y in This.Lines'Range loop
            if This.Lines (Y) = INPUT_LINE then
                State (Y) := To (Current_Datum);
                Current_Datum := Current_Datum + 1;
            end if;
        end loop;

        Do_Apply (This, State);

        -- Copy back the state
        Current_Datum := To'First;
        for Y in This.Lines'Range loop
            if This.Lines (Y) = INPUT_LINE then
                To (Current_Datum) := State (Y);
                Current_Datum := Current_Datum + 1;
            end if;
        end loop;
    end Apply;

    procedure Apply_Full (This : Marbit_Circuit; To : in out Bit_Vector) is
        State : Bit_Vector (1 .. This.Height) := (others => False);

        Current_Datum : Positive := To'First;
    begin
        -- Initialize the state
        for Y in This.Lines'Range loop
            if This.Lines (Y) /= NO_LINE then
                State (Y) := To (Current_Datum);
                Current_Datum := Current_Datum + 1;
            end if;
        end loop;

        Do_Apply (This, State);

        -- Copy back the state
        Current_Datum := To'First;
        for Y in This.Lines'Range loop
            if This.Lines (Y) /= NO_LINE then
                To (Current_Datum) := State (Y);
                Current_Datum := Current_Datum + 1;
            end if;
        end loop;
    end Apply_Full;

    function Expand (
        From : Marbit_Circuit;
        Using : access function (Name : String) return Marbit_Circuit)
        return Marbit_Circuit is

        function Insert_Into (Left, Right : Positive; Circuit : Marbit_Circuit)
            return Marbit_Circuit
        is
            Result : Marbit_Circuit := (
                Height => From.Height + Circuit.Height - Input_Count (Circuit),
                Width => From.Width + Circuit.Width + Left - Right,
                Lines => (others => NO_LINE),
                Steps => (others => (others => NORMAL_STEP)),
                Raw => (others => (others => ' ')));
            Current_Circuit_Row : Positive := 1;
            Current_Result_Row : Positive := 1;

            function Next_Circuit_Real return Line_Kind is
            begin
                for Y in
                    Positive range Current_Circuit_Row .. Circuit.Height
                loop
                    if Circuit.Lines (Y) = INPUT_LINE then
                        return INPUT_LINE;
                    elsif Circuit.Lines (Y) = GARBAGE_LINE then
                        return GARBAGE_LINE;
                    end if;
                end loop;

                return NO_LINE;
            end Next_Circuit_Real;
        begin
            for Y in Positive range 1 .. From.Height loop
                if From.Raw (Y, Left) = '#' then
                    -- Insert from Circuit until input row is next
                    while Circuit.Lines (Current_Circuit_Row) /= INPUT_LINE loop
                        for X in Positive range 1 .. Circuit.Width loop
                            Result.Lines (Current_Result_Row) := NO_LINE;
                            Result.Raw (Current_Result_Row, X + Left - 1) :=
                                Circuit.Raw (Current_Circuit_Row, X);
                        end loop;

                        Current_Result_Row := Current_Result_Row + 1;
                        Current_Circuit_Row := Current_Circuit_Row + 1;
                    end loop;

                    -- Insert input row
                    Result.Lines (Current_Result_Row) := From.Lines (Y);

                    for X in Positive range 1 .. Left - 1 loop
                        Result.Steps (X, Current_Result_Row) :=
                            From.Steps (X, Y);
                        Result.Raw (Current_Result_Row, X) := From.Raw (Y, X);
                    end loop;

                    for X in Positive range 1 .. Circuit.Width loop
                        Result.Steps (X + Left - 1, Current_Result_Row) :=
                            Circuit.Steps (X, Current_Circuit_Row);
                        Result.Raw (Current_Result_Row, X + Left - 1) :=
                            Circuit.Raw (Current_Circuit_Row, X);
                    end loop;

                    for X in Positive range Right + 1 .. From.Width loop
                        Result.Steps (
                            X + Circuit.Width + Left - Right - 1,
                            Current_Result_Row) := From.Steps (X, Y);
                        Result.Raw (
                            Current_Result_Row,
                            X + Circuit.Width + Left - Right - 1) :=
                                From.Raw (Y, X);
                    end loop;

                    Current_Result_Row := Current_Result_Row + 1;
                    Current_Circuit_Row := Current_Circuit_Row + 1;

                    -- Insert from circuit until next real row is input
                    while Next_Circuit_Real /= INPUT_LINE and
                        Current_Circuit_Row <= Circuit.Height
                    loop
                        Result.Lines (Current_Result_Row) :=
                            Circuit.Lines (Current_Circuit_Row);

                        for X in Positive range 1 .. Circuit.Width loop
                            Result.Steps (X + Left - 1, Current_Result_Row) :=
                                Circuit.Steps (X, Current_Circuit_Row);
                            Result.Raw (Current_Result_Row, X + Left - 1) :=
                                Circuit.Raw (Current_Circuit_Row, X);
                        end loop;

                        Current_Result_Row := Current_Result_Row + 1;
                        Current_Circuit_Row := Current_Circuit_Row + 1;
                    end loop;
                else
                    -- Insert row with character at 'Left' Padded across
                    Result.Lines (Current_Result_Row) := From.Lines (Y);

                    for X in Positive range 1 .. Left loop
                        Result.Steps (X, Current_Result_Row) :=
                            From.Steps (X, Y);
                        Result.Raw (Current_Result_Row, X) := From.Raw (Y, X);
                    end loop;

                    for X in
                        Positive range 1 .. Circuit.Width
                    loop
                        Result.Raw (Current_Result_Row, X + Left - 1) :=
                            From.Raw (Y, Left);
                    end loop;

                    for X in Positive range Right + 1 .. From.Width loop
                        Result.Steps (
                            X + Circuit.Width + Left - Right - 1,
                            Current_Result_Row) := From.Steps (X, Y);
                        Result.Raw (
                            Current_Result_Row,
                            X + Circuit.Width + Left - Right- 1) :=
                                From.Raw (Y, X);
                    end loop;

                    Current_Result_Row := Current_Result_Row + 1;
                end if;
            end loop;

            return Result;
        end Insert_Into;

        function Include_At (Top, Left : Positive) return Marbit_Circuit is
            Name_Length : Natural := 0;
        begin
            while Left + Name_Length + 1 <= From.Width and then
                From.Raw (Top, Left + Name_Length + 1) /= '#' loop
                Name_Length := Name_Length + 1;
            end loop;

            if Name_Length = 0 then
                return From;
            end if;

            declare
                Name : String (1 .. Name_Length);
            begin
                for I in Name'Range loop
                    Name (I) := From.Raw (Top, Left + I);
                end loop;

                return Insert_Into (
                    Left,
                    Left + Name_Length + 1,
                    Using.all (Name));
            end;
        end Include_At;
    begin
        for Y in Positive range 1 .. From.Height loop
            if From.Lines (Y) /= NO_LINE then
                for X in Positive range 1 .. From.Width loop
                    if From.Raw (Y, X) = '#' then
                        return Expand (Include_At (Y, X), Using);
                    end if;
                end loop;
            end if;
        end loop;

        return From;
    end Expand;

    procedure Show (
        What : Marbit_Circuit;
        Using : access procedure (Line : String)) is
        Item : String (1 .. What.Width) := (others => ' ');
    begin
        for Y in Positive range 1 .. What.Height loop
            for X in Positive range 1 .. What.Width loop
                Item (X) := What.Raw (Y, X);
            end loop;

            Using.all (Item);
        end loop;
    end Show;
end Marbit_Circuits;
