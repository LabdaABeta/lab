% vi: wrap tabstop=4 shiftwidth=4 expandtab textwidth=0 spell spelllang=en_ca fdm=indent

\documentclass{article}

\usepackage[margin=0.5in]{geometry}

\usepackage{titlesec}
\usepackage{wrapfig}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{array}
\usepackage{booktabs}
\usepackage{caption}
\usepackage{float}
\usepackage{hyperref}
\usepackage{listings}
\usepackage{multirow}
\usepackage{fontspec}
\usepackage[usenames]{xcolor}

\newfontfamily{\unifont}{Unifont}

\titleformat{name=\section}{}{\thetitle.}{0.8em}{\centering\scshape}
\titleformat{name=\section}[block]{}{\thetitle.}{0.8em}{\centering\scshape}
\titleformat{name=\subsection}[display]{}{\thetitle.}{0.5em}{\bfseries}
\titleformat{name=\subsubsection}[runin]{}{\thetitle.}{0.5em}{\itshape}[.]
\titleformat{name=\paragraph,numberless}[runin]{}{}{0em}{}[.]
\titlespacing{\paragraph}{0em}{0em}{0.5em}
\titleformat{name=\subparagraph,numberless}[runin]{}{}{0em}{}[.]
\titlespacing{\subparagraph}{0em}{0em}{0.5em}
\setcounter{secnumdepth}{0} % Prevent numbers on sections

\lstdefinelanguage{lab}
{
 morekeywords={A,B,C,I,J,K,M,N,X,Y,Z,F,S,L,P}, % Registers
 morekeywords=[2]{ADD,MUL,AND,ORR,XOR,MOV,STR,LDA,SUB,BLT,BEQ,BLE,BGT,BNE,BGE,JAL}, % Operations
 sensitive=false,
 otherkeywords={.,+,-,>,>>>,>>>,/,/>,/>>,//,<,<<,<<<,\\,\\<,\\<<,\\\\},
 comment={[l]{\#}, [l]{;}},
}

\definecolor{myblue}{HTML}{2D2F92} % from xcolor
\definecolor{mypurple}{HTML}{67299F}
\definecolor{mypink}{HTML}{A82CA9}
\definecolor{mygrey}{HTML}{949698} % from xcolor
\definecolor{mygreen}{HTML}{2F9F29}
\definecolor{myorange}{HTML}{9F4529}
\lstdefinestyle{lab}{
    basicstyle=\ttfamily,
    identifierstyle=\normalfont,
    keywordstyle={\color{myblue}},
    keywordstyle=[2]{\color{mygreen}},
    commentstyle=\color{mygrey},
    frame=L,
    numbers=left
}

\newcommand{\labcode}[1]{\colorbox{lightgray}{\lstinline[language=lab,style=lab]{#1}}}

\title{LAB Architecture}
\date{\today}
\author{Louis A. Burke}

\begin{document}
\maketitle\clearpage

\section{Registers}

There are 16 register indexes in the LAB processor. These index two sets of registers. The registers are described in table \ref{table:registers}.

\begin{table}
\centering
\begin{tabular}{lll}
    \toprule
    Index & Names & Description \\ \midrule
    0000 & \labcode{0} & Zero Register (read only) \\
    0001 & \labcode{A} & General Purpose \\
    0010 & \labcode{B} & General Purpose \\
    0011 & \labcode{C} & General Purpose \\
    0100 & \labcode{I} & General Purpose \\
    0101 & \labcode{J} & General Purpose \\
    0110 & \labcode{K} & General Purpose \\
    0111 & \labcode{M} & General Purpose \\
    1000 & \labcode{N} & General Purpose \\
    1001 & \labcode{X} & General Purpose \\
    1010 & \labcode{Y} & General Purpose \\
    1011 & \labcode{Z} & General Purpose \\
    1100 & \labcode{F} & Frame Pointer \\
    1101 & \labcode{S} & Stack Pointer \\
    1110 & \labcode{L} & Link Register \\
    1111 & \labcode{P} & Program Counter (read only) \\
\end{tabular}
\caption{Registers}
\label{table:registers}
\end{table}

\section{Operations}

Table \ref{table:operations} summarizes the available operations. In the summaries ð represents the first register (this) while T represents the second register and immediate value (that).

\begin{table}
\centering
\ttfamily
\begin{tabular}{ccl}
    \toprule
    Op. & Code & Description \\ \midrule
    0000 & \labcode{ADD ð, T} & $\eth \gets \eth + T$ \\
    0001 & \labcode{MUL ð, T} & $\eth \gets \eth \times T$ \\
    0010 & \labcode{AND ð, T} & $\eth \gets \eth \land T$ \\
    0011 & \labcode{ORR ð, T} & $\eth \gets \eth \lor T$ \\
    0100 & \labcode{XOR ð, T} & $\eth \gets \eth \oplus T$ \\
    0101 & \labcode{MOV ð, T} & $\eth \gets T$ \\
    0110 & \labcode{STR ð, T} & $[T] \gets \eth$ \\
    0111 & \labcode{LDA ð, T} & $\eth \gets [T]$ \\
    1000 & \labcode{SUB ð, T} & $\eth \gets \eth - T$ \\
    1001 & \labcode{BLT ð, T} & $\eth < 0 \implies P \gets T$ \\
    1010 & \labcode{BEQ ð, T} & $\eth = 0 \implies P \gets T$ \\
    1011 & \labcode{BLE ð, T} & $\eth \le 0 \implies P \gets T$ \\
    1100 & \labcode{BGT ð, T} & $\eth > 0 \implies P \gets T$ \\
    1101 & \labcode{BNE ð, T} & $\eth \ne 0 \implies P \gets T$ \\
    1110 & \labcode{BGE ð, T} & $\eth \ge 0 \implies P \gets T$ \\
    1111 & \labcode{JAL ð, T} & $\eth \gets P \gets T$ \\
\end{tabular}
\caption{LAB Operations}
\label{table:operations}
\end{table}

\section{Operand Shifts}

The value $T$ in the above table is calculated from the second register and immediate value based on the shift-code encoded with the operation. The options are summarized in table \ref{table:shifts}. In this table $R$ is the second register's value and $i$ is the immediate value.

\begin{table}
\centering
\begin{tabular}{ccll}
    \toprule
    Val. & Code & Description & Equation \\ \midrule
    0000 & \labcode{R+i>} & Logical Right Bit Shift & $T := (R + i) \gg 1$ \\
    0001 & \labcode{>R+i} & Arithmetic Right Bit Shift & $T := \frac{R + i}{2}$ \\
    0010 & \labcode{>>R+i} & Arithmetic Right Crumb Shift & $T := \frac{R + i}{4}$ \\
    0011 & \labcode{>>>R+i} & Arithmetic Right Nibble Shift & $T := \frac{R + i}{16}$ \\
    0100 & \labcode{/R+i} & Arithmetic Right Byte Shift & $T := \frac{R + i}{256}$ \\
    0101 & \labcode{/>R+i} & Arithmetic Right Plate Shift & $T := \frac{R + i}{65536}$ \\
    0110 & \labcode{/>>R+i} & Arithmetic Right Dinner Shift & $T := \frac{R + i}{4294967296}$ \\
    0111 & \labcode{//R+i} & Circular Right Bit Rotate & $T := (R + i) \hookrightarrow 1$ \\
    1000 & \labcode{R+i} & No shift & $T := R + i$ \\
    1001 & \labcode{<R+i} & Arithmetic Left Bit Shift & $T := 2(R + i)$ \\
    1010 & \labcode{<<R+i} & Arithmetic Left Crumb Shift & $T := 4(R + i)$ \\
    1011 & \labcode{<<<R+i} & Arithmetic Left Nibble Shift & $T := 16(R + i)$ \\
    1100 & \labcode{\\R+i} & Arithmetic Left Byte Shift & $T := 256(R + i)$ \\
    1101 & \labcode{\\<R+i} & Arithmetic Left Plate Shift & $T := 65536(R + i)$ \\
    1110 & \labcode{\\<<R+i} & Arithmetic Left Dinner Shift & $T := 4294967296(R + i)$ \\
    1111 & \labcode{\\\\R+i} & Circular Left Bit Rotate & $T := (R + i) \hookleftarrow 1$
\end{tabular}
\caption{Operand Shifts}
\label{table:shifts}
\end{table}

\section{Encoding}

The operations are encoded across a word on nibble boundaries. The four least significant nibbles represent the immediate value in signed two's compliment representation. The next least significant nibble represents the second register. The next least significant nibble represents the first register. The second most significant nibble represents the shift code. Finally the most significant nibble represents the operation code.

For reference, this is how the bits would look arranged in a big-endian manner: \texttt{NNNNSSSS AAAABBBB IIIIIIII IIIIIIII}.

When viewed in hexadecimal it may be useful to reference each part by its nibble: \texttt{NSABIIII}.

As an example, consider the operation performed when the instruction loaded is $1516895280$. In hexadecimal bigendian notation this would be \texttt{5A69FC30}. The leading \texttt{5} indicates that this is a \labcode{MOV} operation. The following \texttt{A} indicates that the shift is a left crumb shift. The \texttt{69} indicates that the \emph{this} register is \labcode{K} and the \emph{that} register is \labcode{X}. Finally the \texttt{FC30} indicates that the immediate value is $-976$. Thus the full instruction is \labcode{MOV K, <<X-976}.

\section{Interface}

\subsection{Interrupts}

% Input line. If non-zero then 'teleport' to that PC.
% Output line for current PC. (to be saved)
%
% IRQ:
%   str A, 0+X
%   str B, 0+X+1
%   str C, 0+X+2
%   ...
%
%   Handle request
%
%   lda A, 0+X
%   lda B, 0+X+1
%   ...
%
%   Hardware-specific 'return to user mode'
%
% Also controls "sleep" mode.

\subsection{Memory}

% NOTE: Always read/written in 8 byte amounts. Write masks/translations/etc handled by MMU plugin as described here.
% See: https://stackoverflow.com/questions/48826765
%
% Also must implement mode shifts for protection if desired and in that case hookup to the interrupt line to trigger violations, again if required.
%
% Also has to indicate the PC-increment amount (or assume 4 [byte addressing])


\section{Tools}

\subsection{Modular Implementations}

% Example:
% mkfifo rdmemfifo wrmemfifo intfifo
% l64 intfifo rdmemfifo wrmemfifo &
% lmc64 --load=linker_output.o --interrupt-status-address=0x10 --mode-status-address=0x20 intfifo rdmemfifo wrmemfifo &
% lic64 --interrupt-status-address=0x10 intfifo rdmemfifo wrmemfifo &
% lio64 --stdin-interrupt=1 --stdin-memmap=0xFF00 intfifo rdmemfifo wrmemfifo

\end{document}
