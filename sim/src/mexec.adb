with Marbit_Circuits; use Marbit_Circuits;
with Ada.Text_IO; use Ada.Text_IO;
with Ada.Command_Line; use Ada.Command_Line;

procedure MExec is
    function Read_Circuit return Marbit_Circuit is
    begin
        if End_Of_File then
            return Empty_Circuit;
        else
            declare
                Line : String := Get_Line;
            begin
                return Line + Read_Circuit;
            end;
        end if;
    end Read_Circuit;

    Circuit : Marbit_Circuit := Read_Circuit;
    Arg : String := Argument (1);

    Sym : constant array (Boolean) of Character := (False => '0', True => '1');
begin
    if Arg'Length <= Input_Count (Circuit) then
        declare
            Input : Bit_Vector (1 .. Input_Count (Circuit)) := (others => False);
            Next : Positive := Arg'First;
        begin
            for I of Input loop
                if Next <= Arg'Last then
                    I := Arg (Next) = '1';
                end if;

                Next := Next + 1;
            end loop;

            Apply (Circuit, Input);

            for I of Input loop
                Put (Sym (I));
            end loop;

            Put_Line ("");
        end;
    elsif Arg'Length = Line_Count (Circuit) then
        declare
            Input : Bit_Vector (1 .. Line_Count (Circuit));
            Next : Positive := Arg'First;
        begin
            for I of Input loop
                I := Arg (Next) = '1';
                Next := Next + 1;
            end loop;

            Apply_Full (Circuit, Input);

            for I of Input loop
                Put (Sym (I));
            end loop;

            Put_Line ("");
        end;
    else
        Set_Exit_Status (Failure);
    end if;
end MExec;
