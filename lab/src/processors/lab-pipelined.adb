package body LAB.Pipelined is

    -- Stages

    function PC_Stage (Data : PC_Selection_Data; Bus : Code_Bus)
        return PC_Selection_Results is (
            if Bus.Input.State = Working then (
                PC => Bus.Input.On,
                Bus => (Exists => False))
            else (
                PC => Data.PC,
                Bus => (Exists => True, Address => Data.PC)));

    function IF_Stage (Data : Instruction_Fetch_Data; Bus : Code_Bus)
        return Instruction_Fetch_Results is (
            case Bus.Input.State is
                when Idle | Working => (
                    PC => Data.PC,
                    Next_PC => Data.PC,
                    Ready => False,
                    Raw_Operation => 0),
                when Invalid => (
                    PC => Data.PC,
                    Next_PC => Bus.Input.Handler,
                    Ready => False,
                    Raw_Operation => 0),
                when Valid => (
                    PC => Data.PC,
                    Next_PC => Data.PC + Bus.Input.PC_Increment,
                    Ready => True,
                    Raw_Operation => Bus.Input.Data));

    -- TODO: When declare_expression becomes available use it to make this an
    -- expression function (with delta for reservations)
    function ID_Stage (Data : Instruction_Decode_Data)
        return Instruction_Decode_Results is
        First : constant Register_Index := Register_Index'Val (
            (Data.Raw_Operation / (2 ** 20)) mod (2 ** 4));
        Second : constant Register_Index := Register_Index'Val (
            (Data.Raw_Operation / (2 ** 16)) mod (2 ** 4));
        Valid : constant Boolean := Data.Ready and
            not Data.Reservations (First) and
            not Data.Reservations (Second);
        Immediate : constant Immediate_Value := (
            if Data.Raw_Operation mod (2 ** 16) > 2 ** 15 then
                (Data.Raw_Operation mod (2 ** 16)) - 2 ** 17 else
                Data.Raw_Operation mod (2 ** 16));
        Shift : constant Shift_Kind := Shift_Kind'Val (
            (Data.Raw_Operation / (2 ** 24)) mod (2 ** 4));
        Operation : constant Operation_Kind := Operation_Kind'Val (
            Data.Raw_Operation / (2 ** 28));
        Reservations : Reservation_Page := Data.Reservations;
    begin
        -- This is safe because 0 is never reserved anyways
        Reservations (Freed) := False;

        if Valid then
            Reservations (First) := True;
        end if;

        return (
            PC => Data.PC,
            Valid => Valid,
            First => First,
            Second => Second,
            Immediate => Immediate,
            Shift => Shift,
            Operation => Operation,
            Reservations => Reservations);
    end ID_Stage;

    function RF_Stage (Data : Register_Fetch_Data)
        return Register_Fetch_Results is (
            PC => Data.PC,
            Valid => Data.Valid,
            This => (
                if Data.First = PC then
                    Data.PC
                else
                    Data.Registers (Data.First)),
            That => Apply_Shift (
                To => Data.Registers (Data.Second) + Word (Data.Immediate),
                By => Data.Shift),
            Operation => Data.Operation,
            Target => Data.First);

    function EX_Stage (Data : Execute_Data) return Execute_Results is (
        PC => Data.PC,
        Valid => Data.Valid,
        Data_Address => Data.That,
        Data_Write => Data.Operation = OP_STR,
        Data_Needed => Data.Operation = OP_STR or Data.Operation = OP_LDR,
        Result => (
            case Data.Operation is
                when OP_ADD => This + That,
                when OP_MUL => This * That, -- In HW would be partial
                when OP_AND => This and That,
                when OP_ORR => This or That,
                when OP_XOR => This xor That,
                when OP_SUB => This - That,
                when OP_LDR | OP_STR => This,
                when others => That),
        Branch => (
            case Data.Operation is
                when OP_ADD .. OP_SUB => False,
                when OP_BLT => This < 0,
                when OP_BEQ => This = 0,
                when OP_BLE => This <= 0,
                when OP_BGT => This > 0,
                when OP_BNE => This /= 0,
                when OP_BGE => This >= 0,
                when OP_JAL => True),
        Target => Data.Target);

    function DS_Stage (Data : Data_Selection_Data; Bus : Data_Bus_Input)
        return Data_Selection_Results is (
            if Data.Data_Needed and Data.Valid then (
                if Bus.State = Working and then Bus.On /= Data.Result then (
                    -- We have to wait
                    Bus => (State => Idle),
                    PC => Data.PC,
                    Valid => Data.Valid,
                    Stall => True,
                    Result => Data.Result,
                    Branch => Data.Branch,
                    Target => Data.Target)
                else (
                    -- Set up the read/write
                    Bus => (if Data.Data_Write then (
                        State => Write,
                        Target => Data.Data_Address,
                        Value => Data.Result) else (
                        State => Read,
                        Address => Data.Data_Address)),
                    PC => Data.PC,
                    Valid => Data.Valid,
                    Stall => False,
                    Result => Data.Result,
                    Branch => Data.Branch,
                    Target => Data.Target))
            else (
                Bus => (State => Idle),
                PC => Data.PC,
                Valid => Data.Valid,
                Stall => False,
                Result => Data.Result,
                Branch => Data.Branch,
                Target => Data.Target));






    -- Step function - choreographs stages

        -- TODO: Re-implement as procedure with correct skeleton
    function Step (State : Pipelined_Processor) return Pipelined_Processor is
        Result : Processor_State;

        procedure DS_Stage is
            Datum : Word renames State.Internals.Registers (
                State.Internals.Data_Target);
        begin
            Result.DBus := (
                if State.Internals.Data_Needed then (
                    if State.Internals.Data_Write then
                        (State => Write, Target => EX_Result, Value => Datum)
                    else
                        (State => Read, Address => EX_Result))
                else
                    (State => Idle));
        end DS_Stage;

    begin
        -- These can be parallelized (easy once parallel do is added)
        PC_Stage;
        IF_Stage;
        ID_Stage;
        RF_Stage;
        EX_Stage;
        DS_Stage;
    end Step;
end LAB.Pipelined;
