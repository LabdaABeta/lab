# LAB

This root directory contains the files to build the LAB executor `lab`. Some of
the subdirectories contain the files to build the standard assembler and linker.

All of these programs work with the standard LAB memory array format, which is
a format to represent memory contents.

## Memory Format

The memory format consists of array composites labeled either by absolute range
(as necessary for `lab` input) or by name.

Here is an example:

```
# This is a line comment
0 - 1000 = * 10;
Labelled (100) = 1-3 1, 5 2 4 5, 10-20 3 2, * 10;
```

The grammar is:

```
memory : contents memory | /* epsilon */ ;
contents : location '=' values;
location : range | label;
range : integer '-' integer;
label : text '(' size ')';
values : data more-values;
more-values : ',' values | ';';
data : offset datum;
offset : integer finish | '*';
finish : | '-' integer;
datum : integers;
integers : integer integers | ;
```

The named segments can be used by the linker to piece multiple memory constructs
together. The ranged segments represent the data directly placed in memory on
load.

The integer format is pure hexadecimal.

[//]: # (TODO: Clean up wording and structure it)

When a list of integers appears after a single integer, it naturally extends it.
When a list of integers appears after a range, they are repeated across it.

EG: `1 1 2 3 4 5` creates the data `1, 2, 3, 4, 5` in the first five memory
cells. `1-5 1 2` creates the data `1, 2, 1, 2, 1` in the first five memory
cells.
