with Marbit_Circuits; use Marbit_Circuits;
with Ada.Text_IO; use Ada.Text_IO;

procedure MExpand is
    Stdin : File_Type := Standard_Input;

    function Read_Circuit (From : File_Type := Stdin) return Marbit_Circuit is
    begin
        if End_Of_File (From) then
            return Empty_Circuit;
        else
            declare
                Line : String := Get_Line (From);
            begin
                return Line + Read_Circuit (From);
            end;
        end if;
    end Read_Circuit;

    function Load_Circuit (Name : String) return Marbit_Circuit is
        File : File_Type;
    begin
        Open (File, In_File, Name & ".mb");

        if not Is_Open (File) then
            return Empty_Circuit;
        end if;

        declare
            Result : Marbit_Circuit := Read_Circuit (File);
        begin
            Close (File);
            return Result;
        end;

    exception
        when E : others =>
            return Empty_Circuit;
    end Load_Circuit;

    Circuit : Marbit_Circuit := Read_Circuit;
    Expanded : Marbit_Circuit := Expand (Circuit, Load_Circuit'Access);
begin
    Show (Expanded, Put_Line'Access);
end MExpand;

