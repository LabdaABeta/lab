package LAB.Pipelined is

    -- Already defaulted to initial state
    type Pipelined_Processor is private;

    procedure Step (State : in out Processor_Internals; Bus : in out CPU_Bus);

private

    type PC_Selection_Data is
        record
            Next_PC : Word := 0;
            Branch_Target : Word := 0;
            Take_Branch : Boolean := False;
        end record;

    type PC_Selection_Results is
        record
            Bus : Code_Bus_Output := (Exists => False); --> CBus.Output
            PC : Word := 0; --> IF
        end record;

    type Instruction_Fetch_Data is
        record
            PC : Word := 0; --< PC
        end record;

    type Instruction_Fetch_Results is
        record
            PC : Word := 0; --> ID
            Next_PC : Word := 0; --> PC
            Ready : Boolean := False; --> ID
            Raw_Operation : Operation_Word := 0; --> ID
        end record;

    type Instruction_Decode_Data is
        record
            PC : Word := 0; --< IF
            Ready : Boolean := False; --< IF
            Raw_Operation : Operation_Word := 0; --< IF
            Reservations : Reservation_Page := (others => False); --< ID
            Freed : Register_Index := Zero; --< WB
        end record;

    type Instruction_Decode_Results is
        record
            PC : Word := 0; --> RF
            Valid : Boolean := False; --> RF
            First : Register_Index := Zero; --> RF
            Second : Register_Index := Zero; --> RF
            Immediate : Immediate_Value := 0; --> RF
            Shift : Shift_Kind := None; --> RF
            Operation : Operation_Kind := OP_ADD; --> RF
            Reservations : Reservation_Page := (others => False); --> 1-ID
        end record;

    type Register_Fetch_Data is
        record
            PC : Word := 0; --< ID
            Valid : Boolean := False; --< ID
            First : Register_Index := Zero; --< ID
            Second : Register_Index := Zero; --< ID
            Immediate : Immediate_Value := 0; --< ID
            Shift : Shift_Kind := None; --< ID
            Operation : Operation_Kind := OP_ADD; --< ID
            Registers : Register_Page := (others => 0); --< WB
        end record;

    type Register_Fetch_Results is
        record
            PC : Word := 0; --> EX
            Valid : Boolean := False; --> EX
            This : Word := 0; --> EX
            That : Word := 0; --> EX
            Operation : Operation_Kind := OP_ADD; --> EX
            Target : Register_Index := Zero; --> EX
        end record;

    type Execute_Data is
        record
            PC : Word := 0; --< RF
            Valid : Boolean := False; --< RF
            This : Word := 0; --< RF
            That : Word := 0; --< RF
            Operation : Operation_Kind := OP_ADD; --< RF
            Target : Register_Index := Zero; --< RF
        end record;

    type Execute_Results is
        record
            PC : Word := 0; --> DS
            Valid : Boolean := False; --> DS
            Data_Address : Word := 0; --> DS
            Data_Write : Boolean := False; --> DS
            Data_Needed : Boolean := False; --> DS
            Result : Word := 0; --> DS
            Branch : Boolean := False; --> DS
            Target : Register_Index := Zero; --> DS
        end record;

    type Data_Selection_Data is
        record
            PC : Word := 0; --< EX
            Valid : Boolean := False; --< EX
            Data_Address : Word := 0; --< EX
            Data_Write : Boolean := False; --< EX
            Data_Needed : Boolean := False; --< EX
            Result : Word := 0; --< EX
            Branch : Boolean := False; --< EX
            Target : Register_Index := Zero; --< EX
        end record;

    type Data_Selection_Results is
        record
            Bus : Data_Bus_Output := (Exists => False);
            PC : Word := 0;
            Valid : Boolean := False;
            -- If stall then cancel previous stages and bubble them back
            Stall : Boolean := False; --> Choreographer
            Result : Word := 0;
            Branch : Boolean := False;
            Target : Register_Index := Zero;
        end record;

    -- TODO: Finish cleaning this up and implementing it properly!
    -- Use the same method but be less skittish about flags and out-of-order
    -- data passing

    -- Internals are labeled by who has write access to them
    type Pipelined_Processor is
        record
            PC_Data : PC_Selection_Data;
            IF_Data : Instruction_Fetch_Data;
            ID_Data : Instruction_Decode_Data;
            RF_Data : Register_Fetch_Data;
            EX_Data : Execute_Data;
            DS_Data : Data_Selection_Data;
            DF_Data : Data_Fetch_Data;
            WB_Data : Write_Back_Data;
        end record;

end LAB.Pipelined;
