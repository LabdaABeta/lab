\documentclass{article}

\usepackage[margin=0.5in]{geometry}

\usepackage{amsmath}
\usepackage{array}
\usepackage{booktabs}
\usepackage{caption}
\usepackage{float}
\usepackage{hyperref}
\usepackage{listings}
\usepackage[usenames]{xcolor}

\lstdefinelanguage{lab}
{
 morekeywords={
    r0,r1,r2,r3,r4,r5,r6,r7,lr,fp,sp,pc, % Registers
    dmp,dlp,dmr,dlr,ump,ulp,umr,ulr, % Special Memory Registers
    imr,isa,pit,rtt,ssp,isp,pca,spa, % Special Interrupt Registers
    hih,rth,pih,pah,oah,aah,rah,sih, % Special Handler Registers
    aes,end,rtc,rst,slp,sdm,sum,aei, % Special Status Registers
    add,mul,jmp,cmp,and,orr,xor,nan,ldr,str,pop,psh,mov,ssr,get,put, % Raw instructions
    osr,xsr,msr,fsr,lsr,gsr,jsr,dsr,ksr,esr,hsr,csr,isr,bsr,asr,vsr, % SSR instructions
    jnv,jgt,jeq,jge,jlt,jne,jle,jal, % 000 JMPs
    env,egt,eeq,ege,elt,ene,ele,eal, % 001 JMPs
    cnv,cgt,ceq,cge,clt,cne,cle,cal, % 010 JMPs
    hnv,hgt,heq,hge,hlt,hne,hle,hal, % 011 JMPs
    bnv,bgt,beq,bge,blt,bne,ble,bal, % 100 JMPs
    unv,ugt,ueq,uge,ult,une,ule,ual, % 101 JMPs
    snv,sgt,seq,sge,slt,sne,sle,sal, % 110 JMPs
    rnv,rgt,req,rge,rlt,rne,rle,ral, % 111 JMPs
    swi,hwi, % Interrupts
    nop},
 sensitive=false,
 otherkeywords={(,)},
 comment=[l]{#}
}

\definecolor{grey}{rgb}{0,0.6,0}
\lstset{
    basicstyle=\ttfamily,
    keywordstyle=\color{blue},
    commentstyle=\color{grey},
    frame=L,
    numbers=left
}

\newcommand{\labcode}[1]{\colorbox{lightgray}{\lstinline[language=lab]{#1}}}

\title{LAB Processor -- Overview}
\date{\today}
\author{Louis A. Burke}

\begin{document}
\maketitle\clearpage

\section{Introduction}

The n-bit LAB processor is a general purpose processor specification. It is
designed to be easy to implement (both as simulation and in hardware), easy to
learn, and reasonably fast.

In order to facilitate this goal the instruction set is minimal and memory is
accessed at the word size (which is the size of a register).

\section{Registers}

There are 8 register indexes in the LAB processor. These address 14 registers
and 32 special registers.

The normal registers' values are summarized in \ref{table:registers} while the
special registers' values are summarized in \ref{table:mregisters},
\ref{table:iregisters}, \ref{table:hregisters}, and \ref{table:sregisters}. Only
protected mode code can modify the special register values.

\begin{table}[h!]
\centering
\begin{tabular}{ll}
    \toprule Register & Description \\ \midrule
    \labcode{r0} & Read-only, returns 0 \\
    \labcode{r1} .. \labcode{r3} & General purpose \\
    \labcode{lr} (\labcode{r4}) & Link Register (works mostly as general purpose) \\
    \labcode{fp} (\labcode{r5}) & Frame pointer (works as general purpose) \\
    \labcode{sp} (\labcode{r6}) & Stack pointer (initialized to highest address) \\
    \labcode{pc} (\labcode{r7}) & Program counter \\
\end{tabular}
\caption{LAB Registers}
\label{table:registers}
\end{table}

\begin{table}[h!]
\centering
\begin{tabular}{ll}
    \toprule Register & Description \\ \midrule
    \labcode{DMP} & Driver mode minimum protected address \\
    \labcode{DLP} & Driver mode protected address length \\
    \labcode{DMR} & Driver mode maximum restricted memory address \\
    \labcode{DLR} & Driver mode unrestricted memory length \\
    \labcode{UMP} & User mode minimum protected address \\
    \labcode{ULP} & User mode protected address length \\
    \labcode{UMR} & User mode maximum restricted memory address \\
    \labcode{ULR} & User mode unrestricted memory length \\
\end{tabular}
\caption{LAB Special Memory Registers}
\label{table:mregisters}
\end{table}

\begin{table}[h!]
\centering
\begin{tabular}{ll}
    \toprule Register & Description \\ \midrule
    \labcode{IMR} & Interrupt Mask Register \\
    \labcode{ISA} & Interrupt Status Address \\
    \labcode{PIT} & Programmable Interval Timer (in steps) \\
    \labcode{RTT} & Real Time Timer (in nanoseconds) \\
    \labcode{SSP} & Software SP \\
    \labcode{ISP} & Interrupt SP \\
    \labcode{PCA} & PC Address \\
    \labcode{SPA} & SP Address \\
\end{tabular}
\caption{LAB Special Interrupt Registers}
\label{table:iregisters}
\end{table}

\begin{table}[h!]
\centering
\begin{tabular}{ll}
    \toprule Register & Description \\ \midrule
    \labcode{HIH} & Hardware Interrupt Handler \\
    \labcode{RTH} & Real Time Handler \\
    \labcode{PIH} & Programmable Interval Handler \\
    \labcode{PAH} & Prefetch Abort Handler \\
    \labcode{OAH} & Operation Abort Handler \\
    \labcode{AAH} & Access Abort Handler \\
    \labcode{RAH} & Restriction Abort Handler \\
    \labcode{SIH} & Software Interrupt Handler \\
\end{tabular}
\caption{LAB Special Handler Registers}
\label{table:hregisters}
\end{table}

\begin{table}[h!]
\centering
\begin{tabular}{ll}
    \toprule Register & Description \\ \midrule
    \labcode{AES} & Atomic Element Size \\
    \labcode{END} & Endianness \\
    \labcode{AEI} & Atomic Elements per Instruction \\
    \labcode{RTC} & Real Time Clock \\
    \labcode{RST} & Reset \\
    \labcode{SLP} & Sleep \\
    \labcode{SDM} & Switch to Driver Mode \\
    \labcode{SUM} & Switch to User Mode \\
\end{tabular}
\caption{LAB Special Status Registers}
\label{table:sregisters}
\end{table}

\section{Operations}

\subsection{Summary}

% TODO: Many operations seem unnecessary
% TODO: Look at THUMB
% TODO: Consider PUSH/POP using a mask of registers (bigger immediate)
\begin{table}[H]
\centering
\begin{tabular}{ccll}
    \toprule
    Group      & Op-code & OP            & Description \\ \midrule
    Arithmetic & 0000    & \labcode{ADD} & $A := A + B + I$ \\
               & 0001    & \labcode{MUL} & $A := A * (B + I)$ \\ \midrule
    Branches   & 0010    & \labcode{JMP} & \texttt{if} $A$ \texttt{COND} $PC := B$ \\
               & 0011    & \labcode{CMP} & $A := $ \texttt{CMP} $A, B+I$ \\ \midrule
    Junctions  & 0100    & \labcode{AND} & $A := (A \land B) \ll I$ \\
               & 0101    & \labcode{ORR} & $A := (A \lor B) \ll I$ \\ \midrule
    Logic      & 0110    & \labcode{XOR} & $A := (A \oplus B) \ll I$ \\
               & 0111    & \labcode{NAN} & $A := \neg (A \land B) \ll I$ \\ \midrule
    Memory     & 1000    & \labcode{LDR} & $A := [B + I]$ \\
               & 1001    & \labcode{STR} & $[B + I] := A$ \\ \midrule
    Stack      & 1010    & \labcode{POP} & $A := [B += I]^\dagger$ \\
               & 1011    & \labcode{PSH} & $[B += I] := A^\dagger$ \\ \midrule
    Dataflow   & 1100    & \labcode{MOV} & $A := B + I$ \\
               & 1101    & \labcode{SSR} & $SPR_A I= B$ \\ \midrule
    I/O        & 1110    & \labcode{GET} & $A := \mbox{read}(B + I)$ \\
               & 1111    & \labcode{PUT} & $\mbox{write}(B + I) := A$ \\ \midrule
\end{tabular}
\caption{LAB Operations}
\label{table:operations}
\end{table}
${}^\dagger$ see \labcode{INT}/\labcode{SWI}.


\subsection{Arithmetic}

The arithmetic operations are \labcode{ADD} and \labcode{MUL}. They are pretty
self-explanatory. \labcode{ADD} adds the value in the B register and the
immediate value to the value in the A register. \labcode{MUL} multiplies the
value in the A register by the immediate value added to the value in the B
register.

\subsection{Branches}

The branch operations are composed of a comparison operator and a jump operator.

The \labcode{CMP} comparison instruction directly compares the value in register A
with the immediate value added to the value in register B. If the value in
register A is larger, it is set to 1. If it is smaller it is set to -1. If they
are equal it is set to 0.

The \labcode{JMP} instruction applies the comparison operator specified by the
immediate value to determine whether to jump to the address stored in register B
or to continue execution at the program counter. The highest-order immediate
value bit indicates whether the jump is relative or absolute. If it is a 1 then
the jump will be relative to PC, otherwise it will be an absolute address. The
next bit indicates whether or not to link the jump, that is to overwrite the
value of the link register \labcode{lr} (\labcode{r4}) with the return address
while jumping to the specified address. The next bit, if set, marks a jump as
unlikely to be taken. In the presence of branch prediction this may accelerate
execution. If this bit is not set then nothing is assumed about the branches
behaviour. There is no way to mark a branch as being likely, instead reorganize
code to make a condition on the unlikely path. The remaining three bits encode
the condition on which to jump. The highest order indicates whether or not to
jump on negative values. The second indicates whether or not to jump on zero
values. The third indicates whether to jump on positive values. As a bitmask
this looks like \texttt{RLUCCC}.

An example helps to clarify this. The instruction \labcode{JMP r1, r2, -19} will
be encoded as \texttt{00100010 10101101} (see
\hyperref[sec:encoding]{encoding}). The immediate bits are \texttt{101101} since
this is the 6-bit 2's compliment encoding of $-19$. Breaking this into each bit
we see that the relative and unlikely bits are set, as are the positive and
negative bits. This means that if \labcode{r1} is not zero then \labcode{r2}
will be added to \labcode{pc} to jump execution without saving the return
address anywhere. Additionally the operation is marked as unlikely to occur,
which may impact the branch predictor and pipeline. The assembler has a series
of mnemonics to simplify jump instructions. They are summarized in the tables
below. It can be seen that this instruction could be rewritten as
\labcode{UNE r1, r2}.

\begin{table}[h!]
\centering
\begin{tabular}{lllll}
    \toprule
    Relative & Link & Unlikely & Name    & Mnemonic    \\ \midrule
    No       & No   & No       & Jump    & \texttt{J-} \\
    No       & No   & Yes      & Except  & \texttt{E-} \\
    No       & Yes  & No       & Call    & \texttt{C-} \\
    No       & Yes  & Yes      & Handle  & \texttt{H-} \\
    Yes      & No   & No       & Branch  & \texttt{B-} \\
    Yes      & No   & Yes      & Unusual & \texttt{U-} \\
    Yes      & Yes  & No       & Short   & \texttt{S-} \\
    Yes      & Yes  & Yes      & Recover & \texttt{R-}
\end{tabular}
\caption{LAB Conditional Flag Mnemonics}
\label{table:cflagms}
\end{table}

\begin{table}[h!]
\centering
\begin{tabular}{rll}
    \toprule
    \texttt{CCC} & Condition & Mnemonic       \\ \midrule
    \texttt{000} & Never     & \texttt{-NV} \\
    \texttt{001} & $A > 0$   & \texttt{-GT} \\
    \texttt{010} & $A = 0$   & \texttt{-EQ} \\
    \texttt{011} & $A \ge 0$ & \texttt{-GE} \\
    \texttt{100} & $A < 0$   & \texttt{-LT} \\
    \texttt{101} & $A \ne 0$ & \texttt{-NE} \\
    \texttt{110} & $A \le 0$ & \texttt{-LE} \\
    \texttt{111} & Always    & \texttt{-AL}
\end{tabular}
\caption{LAB Condition Mnemonics}
\label{table:condms}
\end{table}

\subsection{Junctions and Logic}

There are two sections of logical operations due to their ease of implementation
in hardware. These operations apply the given logical operation to the value in
register A and register B. The result is shifted left by the immediate value and
stored back in register A. The \labcode{AND} instruction applies bitwise
conjunction. The \labcode{ORR} instruction applies bitwise disjunction. The
\labcode{XOR} instruction applies exclusive disjunction. The \labcode{NAN}
instruction applies alternative denial.

\subsection{Memory}

The memory operations deal with access and retrieval of data from memory. The
\labcode{LDR} instruction loads the data in memory at the address of the
immediate value added to the value in register B into register A. Meanwhile the
\labcode{STR} instruction stores the value of register A into the same memory
address.

\subsection{Stack}

The stack operations typically operate similarly to the memory operations
with \labcode{POP} corresponding to \labcode{LDR} and \labcode{PSH}
corresponding to \labcode{STR}. The main difference is that instead of merely
adding the immediate value to the value in register B to determine the address,
the immediate value is actually added to register B and saved there. This
can save a \labcode{ADD} operation and facilitates loading of immediate values
(via \labcode{POP r1, pc, -2} for example).

The exception to this behaviour is when the immediate value is zero. In this
case instead of having the same behaviour as the \labcode{LDR} and \labcode{STR}
instructions, the behaviour switches to trigger an interrupt.

In this case a \labcode{POP} instruction becomes a \labcode{SWI} instruction. It
will trigger a software interrupt with the code equal to $A + B + I$.

Similarly \labcode{PSH} becomes a \labcode{HWI} instruction. It will trigger
a hardware interrupt with the code equal to $A + B + I$.

\subsection{Dataflow}

The most commonly used dataflow operation is the \labcode{MOV} instruction. It
puts the immediate value added to the value in register B into register A.

The less common operation is the \labcode{SSR} instruction. Depending on the
immediate value it will apply a different operation to the special register
A with the value of register B.

The four least significant bits indicate whether the resulting bit is 1 or zero
based on whether the A and B are 1 or 0. Thus the least significant bit
indicates the output value when both input bits are zero, the next least
significant bit indicates the output value when A is zero, but B is one, the
next when A is one, but B is zero, and the next when both A and B are one.

The next most significant bits of the immediate indicate which special register
page to access. When zero, they access the special memory registers. When one,
they access the special interrupt registers. When two, they access the special
handler registers. When three, they access the special status registers.

The instruction mnemonics infer the register page by the name of the register.
Meanwhile the other options have their own mnemonics, as described in the table
below. The mnemonics are derived from the single-letter prefix names given on
wikipedia:
\url{https://en.wikipedia.org/wiki/Truth_function#Table_of_binary_truth_functions}

\begin{table}[h!]
\centering
\begin{tabular}{rcll}
    \toprule Immediate Value & Description & Name & Mnemonic \\ \midrule
    ??0000 & $A := 0$ & Contradiction & \labcode{OSR} \\
    ??0001 & $A := \neg A \land \neg B$ & Joint Denial & \labcode{XSR} \\
    ??0010 & $A := \neg A \land B$ & Converse Nonimplication & \labcode{MSR} \\
    ??0011 & $A := \neg A$ & Negation of A & \labcode{FSR} \\
    ??0100 & $A := A \land \neg B$ & Material Nonimplication & \labcode{LSR} \\
    ??0101 & $A := \neg B$ & Negation of B & \labcode{GSR} \\
    ??0110 & $A := A \oplus B$ & Exclusive Disjunction & \labcode{JSR} \\
    ??0111 & $A := A \uparrow B$ & Alternative Denial & \labcode{DSR} \\
    ??1000 & $A := A \land B$ & Conjunction & \labcode{KSR} \\
    ??1001 & $A := A \leftrightarrow B$ & Biconditional & \labcode{ESR} \\
    ??1010 & $A := B$ & Proposition B & \labcode{HSR} \\
    ??1011 & $A := \neg (A \land \neg B)$ & Material Implication & \labcode{CSR} \\
    ??1100 & $A := A$ & Proposition A & \labcode{ISR} \\
    ??1101 & $A := \neg (\neg A \land B)$ & Converse Implication & \labcode{BSR} \\
    ??1110 & $A := A \lor B$ & Disjunction & \labcode{ASR} \\
    ??1111 & $A := 1$ & Tautology & \labcode{VSR} \\
\end{tabular}
\caption{MSR Mnemonics}
\label{table:msrmn}
\end{table}

\subsection{Input and Output}

The two I/O operations are \labcode{GET} and \labcode{PUT}. These operate
identically to their memory counterparts \labcode{LDR} and \labcode{STR} except
they use the address bus rather than the data bus. See
\url{https://stackoverflow.com/questions/3215878/what-are-in-out-instructions-in-x86-used-for}
for an explanation of their usefulness. Essentially these allow memory-mapped
I/O devices to occupy addresses that would normally be valid program memory.

Both of these operations trigger an Operation Abort interrupt if executed in
user mode.

\section{Encoding}
\label{sec:encoding}

Each operation is encoded across 16 bits.

The most significant nibble indicates the instruction opcode. The next three
bits encode the A register index, then the next three the B register index. The
remaining 6 bits encode the immediate value as a 2's compliment number.

This can be viewed as: \texttt{NNNN AAAB BBII IIII}

\section{Modes}

The LAB CPU has four modes, protected mode, driver mode, user mode, and sleep
mode.

The CPU starts in protected mode. In this mode all memory is writable, all
special registers are accessible, and all operations are usable.

Driver mode is not able to access the special registers, but can still
perform I/O. It has its own protected memory space as well as its own register
file.

User mode can neither access special registers, nor I/O operations. It also has
its own protected memory space.

Sleep mode is a state where the processor uses minimal power while awaiting
interaction.

\subsection{Memory Spaces}

Both driver and user modes have their own protected and unrestricted memory
spaces. These are defined using four special memory registers. Protected memory
can be executed while unrestricted memory can be read from and written to.

The first register is the minimum protected address. This is the lowest address
which can be executed without triggering an interrupt.

The next register is the protected address length. This is the length of the
protected address space. Memory after this many bytes from the minimum protected
address will trigger an interrupt if executed.

The third register is the maximum restricted memory address. This is the highest
address which will cause an interrupt if accessed. All memory accesses in the
given mode will have this value added to them. Thus any memory access to memory
$0$ will always trigger an interrupt, as this is the maximum restricted address.

The final register is the unrestricted memory length. This is the length of
unrestricted memory before memory is restricted again.

The CPU does not have a built-in memory management unit, however the addressing
modes are conducive to the addition of one.

\section{Interrupts}

The LAB CPU supports regular interrupts as well as one programmable interval
timer and one real time clock.

\subsection{Interrupt List}

There are 8 predefined interrupts which can have handlers associated with them.

The first is the hardware interrupt. It is triggered by any hardware signal as
processed by the vectored interrupt controller (VIC).

The second is the real time interrupt. It is triggered after a certain period of
real time has elapsed.

The third is the programmable interval timer interrupt. It is triggered when
the programmable timer reaches 0.

The fourth is the prefetch abort. It is triggered when the memory to be loaded
by the program counter is invalid. This can occur, for example, if the program
counter were to be loaded with a negative value.

The fifth is the operation abort. It is triggered when user mode code attempts
to execute any operation involving I/O.

The sixth is the access abort. It is triggered when either user mode code or
driver mode code attempts to access a special register.

The seventh is the restriction abort. It is triggered when an attempt is made to
either read or write restricted memory.

The eighth is the software interrupt. It is triggered by an explicit software
interrupt operation.

\subsection{Hardware Interrupts}

When an interrupt appears at the VIC the following steps happen.

First the interrupt mask register (\labcode{IMR}) is inspected. If the bit
corresponding to the current interrupt is not set, then processing stops and no
interrupt is raised.

Next the address in the hardware interrupt handler (\labcode{HIH}) is inspected.
If it is zero then the interrupt is cleared and processing stops.

Finally the driver SP is loaded with the value stored in the interrupt SP
register (\labcode{ISP}) and the driver PC is loaded with the value stored in
the hardware interrupt handler. The address in the interrupt status address
(\labcode{ISA}) is loaded with the VIC's interrupt status word. The interrupt is
then cleared on the VIC. Finally the mode is switched to driver and execution
continues.

\subsection{Real Time Interrupts}

The real time timer is an unsigned register whose value decreases at a rate of
once every nanosecond until it becomes zero.

When the real time timer register reaches zero (edge triggered), the real time
handler (\labcode{RTH}) is inspected. If it is zero then the timer is ignored.

Otherwise, the driver SP is loaded with the value stored in the interrupt SP
register (\labcode{ISP}) and the driver PC is loaded with the value stored in
the real time handler. Finally the mode is switched to driver and execution
continues.

\subsection{Programmable Interval Timer}

The programmable interval timer is an unsigned register whose value decreases at
a rate of once every CPU cycle. That is, it decreases whenever an operation is
completed. Until it becomes zero.

When the programmable interval timer reaches zero (edge triggered), the
programmable interval handler (\labcode{PIH}) is inspected. If it is zero then
the timer is ignored.

Otherwise, the driver SP is loaded with the value stored in the SP register
(\labcode{ISP}) and the driver PC is loaded with the value stored in the
programmable interval handler. Finally the mode is switched to driver and
execution continues.

\subsection{Aborts}

When any of the aborts occurs, whether it be a prefetch abort, operation abort,
access abort, or restriction abort, the result is the same. The operation does
not even begin, the PC is written to the PC address then overwritten with the
appropriate handler.  The SP is written to the SP address and then overwritten
with the Software SP (\labcode{SSP}). If the appropriate handler is zero, then
the system halts; otherwise execution continues in privileged mode.

\subsection{Software Interrupt}

When a software interrupt occurs the operation is completed. The PC is written
to the PC address then overwritten with the software interrupt handler
(\labcode{SIH}). The SP is written to the SP address and then overwritten with
the Software SP (\labcode{SSP}). The address in the interrupt status address
(\labcode{ISA}) is loaded with the interrupt value. If the software interrupt
handler is zero, then the system halts; otherwise execution continues in
privileged mode.

\section{Status Modification}

The special status registers allow for modification of the system state.

\subsection{Endianness}

Writing to the atomic element size indicates what size chunks the system should
use when reading from and writing to memory.

The order in which a register's chunks are read or written is determined by the
endianness register. Each chunk is read or written in-order based on the chunk's
value in the word.

An example is helpful. Setting \labcode{AES} to 8 and \labcode{END} to the
hexadecimal value 02010403 will read and write 32-bit registers in what is known
as PDP-endian format. Any zeroed atomic elements are masked away and not
written. The highest valued element indicates how many addresses to write to.

The subtlety of these registers is also present in that the system need not be
bit-oriented. Whatever the underlying representation of the system, the
\labcode{AES} register indicates how many atomic elements fit in each address
and the \labcode{END} register indicates in what order to write them into
memory. It is up to user programs to increment memory addresses appropriately.

Here are some more unusual settings and how they would be interpreted:

% TODO

Due to the more complicated nature of these registers, they may not always be
fully implemented. They are to be shared with the memory management unit which
may use it as it likes.

\subsection{Real Time Clock}

The real time clock is accessible via the \labcode{RTC} special register.
Whenever any value is written to this register it is interpreted as a memory
address and the current value of the register is written to that address. Then
the register's value is reset to 0. It will increase by 1 every nanosecond.

\subsection{Reset}

Writing any value to the reset (\labcode{RST}) special register will cause the
CPU to reset. This will restore all registers to their initial states, it will
not necessarily zero memory.

\subsection{Mode Switch}

The sleep, switch to driver mode, and switch to user mode registers all change
the current mode of the system. Sleep mode will suspend execution until an
interrupt is processed. Driver mode will switch to driver mode with the value as
PC. Similarly user mode will switch to user mode with the value as PC.

\subsection{AEI}

This register is used directly as the amount by which to increment the program
counter after each instruction. It indicates how many atomic elements (memory
addresses) are consumed by each instruction.

\section{Tools}

This project comes with a number of tools for working with the LAB architecture.
Some are to assist in implementation of LAB machines while some assist in
development for such machines.

\subsection{Implementations}

Different implementations of the LAB processor are a part of this project. These
are written in Ada and export a usable interface for both Ada and C while also
compiling a CLI implementation. Additionally, work is in progress for logic gate
level VHDL implementations and reversible computation models.

The interface is parameterized by 5 aspects. The first is the bit-size of the
registers. The second is the form of memory. The third is the form of
interrupts. The fourth is the form of state initialization. Finally the fifth is
the timing system, which integrates with all of the previous systems.

\subsubsection{Register Size}

The Ada interface supports native register sizes up to the register size of the
target platform. Additionally it exports an interface for custom registers so
long as they define the required operations.

\subsubsection{Memory Type}

The Ada interface exposes multiple forms of memory. The first is as an array in
memory. The second is a splay tree, for more dynamic memory usage (using only as
much as is necessary). Finally like with registers a custom memory system can be
used so long as it defines the required operations.

\subsubsection{Interrupt Format}

The Ada interface exposes interrupts in three ways. The first is via callbacks
passed to the instantiation. The second is via callbacks passed to the generic
instantiations. The final interface is via a simulated databus passed by
address to the implementation. This exposes the 'read' and 'write' lines as well
as a data line as a register within the record.

\subsubsection{Initialization}

The Ada interface supports initialization in three ways. First is via a raw memory
map array. The second is via a map of address-value pairs dynamically sized to
fit. The memory map also supports continuous ranges. Both methods are supported
by the LAB executable file format, which all precompiled implementations load.
These executables can be created using the provided linker.

\subsubsection{Precompiled Implementations}

The precompiled implementations are:

\begin{table}[h!]
\centering
\begin{tabular}{rccccl}
    \toprule
    Name & Register Size & Memory Type & I/O and Interrupts & Executable Name & Timing \\ \midrule
    Standard 32-bit & 32-bit & Splay Tree & stdio & l32 & Optional Log File \\
    Standard 64-bit & 64-bit & Splay Tree & stdio & l64 & Optional Log File \\
    Standard Fast & 64-bit & Array & stdio & lab & Optional Log File \\
    Net 32-bit & 32-bit & Splay Tree & Net Ports & ln32 & Optional Log Port \\
    Net 64-bit & 64-bit & Splay Tree & Net Ports & ln64 & Optional Log Port \\
\end{tabular}
\caption{Precompiled LAB Implementations}
\label{table:prelab}
\end{table}

\subsection{Linker}

The linker processes compiled LAB object files and converts them into an LAB
executable file. The LAB executable file format is a hybrid Text/Binary format.
The content until the first line not starting with a '\#' is ignored. After that
the binary executable format begins. This allows for comments and remarks to be
embedded directly into the executable, as well as a shebang line if desired.

% TODO: This won't work, just use a pure binary with a flag to ignore first if
% needed

The remainder of the file presents a memory map.
\subsection{Assembler}
\subsection{LAB Low Level Language} % TODO: Like Ada, but ... (see phone)
\subsection{C Compiler}

\end{document}
