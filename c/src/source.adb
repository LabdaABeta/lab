with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;

package body Source is
    function "=" (Left, Right : Character) return Boolean is begin
        return Left.File_Name = Right.File_Name and Left.Row = Right.Row and
            Left.Column = Right.Column and Left.Value = Right.Value;
    end "=";

    function "=" (Left : Character; Right : Standard.Character) return Boolean
    is begin
        return Left.Value = Right;
    end "=";

    function "=" (Left : Standard.Character; Right : Character) return Boolean
    is begin
        return Left = Right.Value;
    end "=";

    function Image (C : Character) return Standard.String is begin
        return To_String (C.File_Name) &
            "|" & Positive'Image (C.Row) &
            " col" & Positive'Image (C.Column) &
            "| " & C.Value;
    end Image;
end Source;
