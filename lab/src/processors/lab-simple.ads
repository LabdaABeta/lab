package LAB.Simple is

    -- Already defaulted to initial state
    type Simple_Processor is private;

    procedure Step (State : in out Simple_Processor; Bus : in out CPU_Bus);

private

    type Processor_State is (
        Prefetch,   -- Need to ask the Code Bus for an operation
        Execute,    -- Need to receive an operation from Code Bus
        Writeback); -- Need to write results to register page

    type Simple_Processor is
        record
            Registers : Register_Page := (others => 0);
            State : Processor_State := Prefetch;
            Result : Word := 0;
            Target : Register_Index := Zero;
            Branch : Boolean := False;
            Data : Boolean := False;
        end record;

end LAB.Simple;



